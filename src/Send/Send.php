<?php
/**
 * User: szliugx@gmail.com
 * Date: 2017/7/4
 * Time: 17:48
 */

namespace YouBanMonitor\Send;

use YouBanMonitor\Core\MonitorInfo;
use YouBanMonitor\Core\Udp;

class Send
{
    protected $monitorInfo;

    public function __construct(MonitorInfo $monitorInfo)
    {
        $this->monitorInfo = $monitorInfo;
    }

    public function udp()
    {
        $udp = new Udp();
        $udp->udp($this->monitorInfo->getContent(),$this->monitorInfo->getAddress(),$this->monitorInfo->getPort());
    }
}