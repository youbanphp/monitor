<?php
/**
 * User: szliugx@gmail.com
 * Date: 2017/7/4
 * Time: 17:35
 */

namespace YouBanMonitor\Foundation\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use YouBanMonitor\Send\Send;

class SendServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $pimple['send'] = function ($pimple) {
            return new Send($pimple['monitor_info']);
        };
    }
}