<?php
/**
 * User: szliugx@gmail.com
 * Date: 2017/7/4
 * Time: 17:20
 */

namespace YouBanMonitor\Foundation;

use Pimple\Container;
use YouBanMonitor\Core\MonitorInfo;

/**
 * Class Application.
 *
 * @property \YouBanMonitor\Send\Send                $send
 *
 */
class Application extends Container
{
    protected $providers = [
        ServiceProviders\SendServiceProvider::class,
    ];

    protected $monitorInfo;

    public function __construct(array $configs)
    {
        parent::__construct();
        $this['monitor_info'] = New MonitorInfo($configs['address'], $configs['port'], $configs['content']);

        $this->registerProviders();
    }

    /**
     * @return array
     */
    public function getProviders()
    {
        return $this->providers;
    }

    /**
     * @param array $providers
     */
    public function setProviders($providers)
    {
        $this->providers = $providers;
    }

    /**
     * Magic get access.
     *
     * @param string $id
     *
     * @return mixed
     */
    public function __get($id)
    {
        return $this->offsetGet($id);
    }

    /**
     * Magic set access.
     *
     * @param string $id
     * @param mixed  $value
     */
    public function __set($id, $value)
    {
        $this->offsetSet($id, $value);
    }

    /**
     * Register providers.
     */
    private function registerProviders()
    {
        foreach ($this->providers as $provider) {
            $this->register(new $provider());
        }
    }
}
