<?php
/**
 * User: szliugx@gmail.com
 * Date: 2017/7/4
 * Time: 17:59
 */

namespace YouBanMonitor\Core;

class MonitorInfo
{
    protected $address;

    protected $port;

    protected $content;

    /**
     * MonitorInfo constructor.
     * @param $address
     * @param $port
     * @param $content
     */
    public function __construct($address, $port, $content)
    {
        $this->address = $address;
        $this->port    = $port;
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param mixed $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

}
