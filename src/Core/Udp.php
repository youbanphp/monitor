<?php
/**
 * User: szliugx@gmail.com
 * Date: 2017/7/4
 * Time: 18:20
 */

namespace YouBanMonitor\Core;

class Udp
{
    public function udp($input, $server, $port)
    {
        try {
            //创建一个socket
            $sock = socket_create(AF_INET, SOCK_DGRAM, 0);

            //发送数据到这个socket
            socket_sendto($sock, $input, strlen($input), 0, $server, $port);

            //关闭创建的socket
            socket_close($sock);

            //返回
            return true;
        } catch (\Exception $e) {
            $errorCode = socket_last_error();
            $errorMsg  = socket_strerror($errorCode);
            //throw new \ErrorException($e->getMessage());
        }
    }
}
