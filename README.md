#游伴应用监控
简单的使用方法

```php
<?php  
/**
 * User: szliugx@gmail.com
 * Date: 2017/7/4
 * Time: 18:29
 */
  
include "./vendor/autoload.php";
use YouBanMonitor\Foundation\Application;
  
$options = [
    'address' => '127.0.0.1',
    'port'    => '8089',
    'content' => 'test:content',
];
  
$app = new Application($options);
$app->send->udp();
````
  
未完待续......